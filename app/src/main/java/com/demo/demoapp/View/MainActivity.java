package com.demo.demoapp.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.demo.demoapp.Model.AddressInfo;
import com.demo.demoapp.Presenter.Map.MapContract;
import com.demo.demoapp.Presenter.Map.MapPresenter;
import com.demo.demoapp.R;
import com.huawei.hms.maps.CameraUpdate;
import com.huawei.hms.maps.CameraUpdateFactory;
import com.huawei.hms.maps.HuaweiMap;
import com.huawei.hms.maps.MapView;
import com.huawei.hms.maps.OnMapReadyCallback;
import com.huawei.hms.maps.model.BitmapDescriptorFactory;
import com.huawei.hms.maps.model.CameraPosition;
import com.huawei.hms.maps.model.LatLng;
import com.huawei.hms.maps.model.Marker;
import com.huawei.hms.maps.model.MarkerOptions;

/**
 * map activity entrance class
 */
public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, MapContract.HandlingView {

    private MapView mMapView;
    private CardView getLocation;
    private ConstraintLayout alertLayout;
    private ConstraintLayout mapLayouy;
    MapContract.HandlingPresenter mapPresenter;
    private static final String TAG = "MapViewDemoActivity";
    //Huawei map
    private HuaweiMap hMap;
    private Marker currentLocationMarker;
    private Marker pinMarker;
    private LatLng currentLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // init view
        mMapView = findViewById(R.id.mapView);
        getLocation = findViewById(R.id.getLocation);
        alertLayout = findViewById(R.id.alertLayout);
        mapLayouy = findViewById(R.id.mapLayouy);
        // on get location button click (get current location)
        getLocation.setOnClickListener(click -> {
            if (currentLocation != null) {
                changeCameraPosation(currentLocation);
            } else {
                mapPresenter.initLocationKit();
            }
        });
        // init map presenter
        mapPresenter = new MapPresenter(this, this, this);
        //init map
        mapPresenter.initMap(savedInstanceState);
    }


    @Override
    public void onMapReady(HuaweiMap map) {
        //get map instance in a callback method
        Log.d(TAG, "onMapReady: ");
        hMap = map;
        hMap.setMyLocationEnabled(false);
        hMap.getUiSettings().setMyLocationButtonEnabled(true);
        //get current location
        mapPresenter.initLocationKit();
        // set custom view to info window
        hMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(MainActivity.this));
        //move pin on clicked on map
        hMap.setOnMapClickListener(latLng -> mapPresenter.getLocationInformation("movePin", latLng));
        //on drag marker ( on start drag remove info window then on drag end view info window with new info position )
        hMap.setOnMarkerDragListener(new HuaweiMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                marker.hideInfoWindow();
            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                mapPresenter.getLocationInformation("movePin", marker.getPosition());
            }
        });
        // check if info window open or close and do the reverse
        hMap.setOnMarkerClickListener(marker -> {
            if (marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
            } else {
                marker.showInfoWindow();
            }
            return false;
        });
    }

    @Override
    public void showMap(Bundle mapViewBundle) {
        if (alertLayout.getVisibility() == View.VISIBLE) {
            alertLayout.setVisibility(View.GONE);
            mapLayouy.setVisibility(View.VISIBLE);
        }
        mMapView.onCreate(mapViewBundle);
        //get map instance
        mMapView.getMapAsync(this);
    }

    //update camera posation
    @Override
    public void changeCameraPosation(LatLng latLng) {
        CameraPosition cameraPosition = new CameraPosition(latLng, 15.0f, 2.0f, 2.0f);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        hMap.animateCamera(cameraUpdate);
        hMap.moveCamera(cameraUpdate);
    }

    //on get current location or map click
    @Override
    public void LocationInfo(String type, LatLng latLng, AddressInfo addressInfo) {
        switch (type) {
            case "current":
                currentLocation = latLng;
                createStarMark(latLng, addressInfo);
                if (pinMarker == null) {
                    mapPresenter.showPin(latLng.latitude + 0.01, latLng.longitude + 0.01);
                }
                break;
            case "movePin":
                MovePinMark(latLng, addressInfo);
                break;
            case "showPin":
                showPinMark(latLng, addressInfo);
                break;
        }
    }

    //create star mark for current location
    private void createStarMark(LatLng latLng, AddressInfo addressInfo) {
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.star))
                .title(addressInfo.getFormatAddress())
                .snippet(addressInfo.getCountry());
        if (null != currentLocationMarker) {
            currentLocationMarker.remove();
        }
        currentLocationMarker = hMap.addMarker(options);
    }

    //moving pin when user drag or click
    private void MovePinMark(LatLng latLng, AddressInfo addressInfo) {
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin))
                .title(addressInfo.getFormatAddress())
                .snippet(addressInfo.getCountry())
                .draggable(true);
        if (null != pinMarker) {
            pinMarker.remove();
        }
        pinMarker = hMap.addMarker(options);
        pinMarker.showInfoWindow();
    }

    //show pin on create map
    private void showPinMark(LatLng latLng, AddressInfo addressInfo) {
        MarkerOptions options = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.pin))
                .title(addressInfo.getFormatAddress())
                .snippet(addressInfo.getCountry())
                .draggable(true);
        if (null != pinMarker) {
            pinMarker.remove();
        }
        pinMarker = hMap.addMarker(options);
    }


    //get info of current location
    @Override
    public void currentLocation(LatLng latLng) {
        mapPresenter.getLocationInformation("current", latLng);
    }

    @Override
    public void viewAlertMessage() {
        if (mapLayouy.getVisibility() == View.VISIBLE) {
            alertLayout.setVisibility(View.VISIBLE);
            mapLayouy.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mMapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mMapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapPresenter.removeLocationUpdatesWithCallback();
        mMapView.onDestroy();
    }

    @Override
    protected void onPause() {
        mMapView.onPause();
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();
    }

}

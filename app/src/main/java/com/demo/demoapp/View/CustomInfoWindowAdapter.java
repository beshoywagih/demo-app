package com.demo.demoapp.View;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.demo.demoapp.R;
import com.huawei.hms.maps.HuaweiMap;
import com.huawei.hms.maps.model.Marker;

class CustomInfoWindowAdapter implements HuaweiMap.InfoWindowAdapter {
    private final View mWindow;
    CustomInfoWindowAdapter(Activity activity) {
        mWindow = activity.getLayoutInflater().inflate(R.layout.location_info_design, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        TextView txtvTitle = mWindow.findViewById(R.id.addressInfo);
        TextView txtvSnippett = mWindow.findViewById(R.id.addressCountry);
        txtvTitle.setText(marker.getTitle());
        txtvSnippett.setText(marker.getSnippet());
        return mWindow;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
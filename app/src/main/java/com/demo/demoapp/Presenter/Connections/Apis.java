package com.demo.demoapp.Presenter.Connections;

import com.google.gson.JsonObject;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface Apis {

    @POST("mapApi/v1/siteService/reverseGeocode")
    Observable<Response<ResponseBody>> getLocationInfo(@Query(value = "key", encoded = true) String key,
                                                       @Body JsonObject siteKitRequest);
}

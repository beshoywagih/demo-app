package com.demo.demoapp.Presenter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class NetworkChangeReceiver extends BroadcastReceiver {
    PublishSubject<Boolean> networkState = PublishSubject.create();

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final ConnectivityManager connMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isConnected() || mobile.isConnected()) {
            Log.e("online","online");
            networkState.onNext(true);
        }else {
            networkState.onNext(false);
            Log.e("offline","offline");
        }
    }

    public Observable<Boolean> getNetworkState() {
        return networkState.hide();
    }


}